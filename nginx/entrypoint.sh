#!/usr/bin/env bash

echo "[ReverseProxy] Waiting 3 seconds for dependencies..."

sleep 3

echo "[ReverseProxy] Starting NGINX ReverseProxy..." 

/usr/sbin/nginx -g "daemon off;"